# Introduction to using a Debian System


## Goals

_Class notes_

1. Getting an image
2. Making a bootable image
3. Finding Installation documents
4. Logging in the first time
5. The desktop environment
6. Adding more applications
7. The command line
8. Email and the web browser
9. The cloud and other magic
10. Where to go next?

## Header 2

## Header 3

## Header 4


----
David (KC4ZVW)
